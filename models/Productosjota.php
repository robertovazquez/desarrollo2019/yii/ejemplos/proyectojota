<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productosjota".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $foto
 * @property string|null $descripcion
 * @property int|null $precio
 * @property int|null $oferta
 */
class Productosjota extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productosjota';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio', 'oferta'], 'integer'],
            [['nombre', 'foto'], 'string', 'max' => 20],
            [['descripcion'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'foto' => 'Foto',
            'descripcion' => 'Descripcion',
            'precio' => 'Precio',
            'oferta' => 'Oferta',
        ];
    }
}
