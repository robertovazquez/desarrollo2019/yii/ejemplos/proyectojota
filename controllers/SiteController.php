<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Productosjota;
use app\models\categorias;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionInicio()
    {
        return $this->render('index');
    }

     public function actionOfertas()
    {
        $resultado=Productosjota::find()->where(["oferta"=>true]);
        $dataProvider = new ActiveDataProvider([
         'query' => $resultado,
        'pagination' => [
        'pageSize' => 20,
    ],
]);
        
        return $this->render('ofertas',[
            "data"=>$dataProvider,
        ]);
    }
    
   public function actionProductos()
    {
       
       
        $resultado1=Productosjota::find();
        
        $dataProvider = new ActiveDataProvider([
         'query' => $resultado1,
        'pagination' => [
        'pageSize' => 20,
    ],
]);
        
         return $this->render('productos',[
            "data"=>$dataProvider,
        ]);
    }

     public function actionCategorias()
    
           {
       
       
        $resultado2=categorias::find();
        
        $dataProvider = new ActiveDataProvider([
         'query' => $resultado2,
        'pagination' => [
        'pageSize' => 20,
    ],
]);
        
         return $this->render('categorias',[
            "data"=>$dataProvider,
        ]);
    }

         
         
  public function actionNosotros()
    {
        return $this->render('nosotros');
    } 
      public function actionContacto()
    {
        return $this->render('contacto');
    } 
    
    
    
    
    
    
    
}
