<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<style type="text/css">
    
    h3{
        
        font-size: 20px;
        text-align: center;
        color: red;
    }
    
</style>
<div class="col-sm-6 col-md-4">
     
    <div class="thumbnail">
      <h3>Producto en oferta</h3>
      <div class="caption">
        <h4><?= $model->id ?></h4>
        <p><?= $model->nombre ?></p>
        <p><?=Html::img("@web/imgs/".$model->foto)?></p>
         <p><?=$model->descripcion?></p>
         <p><?=$model->oferta?></p>
      </div>
    </div>
  </div>
