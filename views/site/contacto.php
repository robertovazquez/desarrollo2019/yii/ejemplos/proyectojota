<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        
        <title></title>
        <style type="text/css">
            
            .container{box-sizing:border-box;
  
}

form{
	width:300px;
	padding:16px;
	border-radius:10px;
	margin:auto;
	background-color:#ccc;
}

form label{
	width:72px;
	font-weight:bold;
	display:inline-block;
}

form input[type=text], form input[type=email]{
	width:180px;
	padding:3px 10px;
	border:1px solid #f6f6f6;
	border-radius:3px;
	background-color:#f6f6f6;
	margin:8px 0;
	display:inline-block;
}

form input[type=submit]{
	width:100%;
	padding:8px 16px;
	margin-top:32px;
	border:1px solid #000;
	border-radius:5px;
	display:block;
	color:#fff;
	background-color:#000;
} 

form input[type=submit]:hover{
	cursor:pointer;
}

textarea{
	width:100%;
	height:100px;
	border:1px solid #f6f6f6;
	border-radius:3px;
	background-color:#f6f6f6;			
	margin:8px 0;
	/*resize: vertical | horizontal | none | both*/
	resize:none;
	display:block;
}

h1{
	text-align:center;
	color:white;
	font-style:italic;
	font-family:"Century Gothic";
	margin:50px;
}

.formulario{
   margin:auto;
}

            
            
            
            
            
            
        </style>
        
        
        
    </head>
    <body>
        <div class="container">

 <div class="row">
 
    <div class="col-md-12 col-12">
    
            <h1>Formulario de registro</h1>
    
          <div class="formulario">
       
            <form action="#" target="" method="get" name="formDatosPersonales">

	<label for="nombre">Nombre</label>
	<input type="text" name="nombre" id="nombre" placeholder="Escribe tu nombre"/>

	<label for="apellidos">Apellidos</label>
	<input type="text" name="apellidos" id="apellidos" placeholder="1r Apellido"/>

	<label for="email">Email</label>
	<input type="email" name="email" id="email" placeholder="email" required />

	<label for="asunto">Asunto</label>
	<input type ="text" name="asunto" id="asunto" placeholder="titular de la consulta"/>

	<label for="mensaje">Mensaje</label>
	<textarea name="mensaje" for="mensaje" placeholder="describe brevemente en menos de 300 carácteres" maxlength="300"></textarea>
	
	<input type="submit" name="enviar" value="enviar datos"/>
</form>

     

       
    </div>

    
    
    
    
 </div>
 </div>
</div>

        
    </body>
</html>
