<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>


<div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      
      <div class="caption">
        <h3><?= $model->id ?></h3>
        <p><?= $model->nombre ?></p>
        <p><?=Html::img("@web/imgs/".$model->foto)?></p>
         <p><?=$model->descripcion?></p>
         <p><?=$model->precio?></p>
        
      </div>
    </div>
  </div>
