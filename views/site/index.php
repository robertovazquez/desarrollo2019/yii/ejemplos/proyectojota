<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'My Yii Application';
?>

<div class="site-index">

    <div class="jumbotron">
        <h1>JOTA</h1>

        <p class="lead">Tienda dedicada a la venta de productos tecnologicos y visuales</p>

       
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
              

                <p>Si hay una temática donde existen miles de tiendas en internet es en la tecnología. Por esta razón, al existir tantas tiendas con miles de productos, es importante 
                    comparar y ver en qué producto destaca cada una de ellas y cuál tiene el mejor precio para el artículo que queramos comprar.
Existen tiendas de reconocido prestigio como Amazon, Redcoon, Fnac, Pccomponenetes… en las que puedes comprar con total confianza ya que tienen un historial impecable y son reconocidas
por todo el mundo. Sin embargo, si por alguna razón te ves tentado a comprar en una tienda que no conoces de nada o que nunca has oido hablar de ella, nuestra recomendación es que
busques información de ella en internet y leas opiniones de la gente antes de decidirte a comprar cualquier artículo.
Si existe alguna temática de productos donde abundan las tiendas falsas que aparecen y desaparecen como por arte de magia esa es sin duda la tecnología. Por norma general los aparatos
electrónicos son muy jugosos porque tienen un precio alto y cualquier descuento o precio atractivo que se encuentre en la red llama la atención y tienta mucho su compra si es una oferta
interesante. Esto sirve de gancho en muchas ocasiones para engañar al consumidor y aprovecharse de él. Por esta razón, recomendamos siempre comprar en tiendas con buena reputación online o por lo menos informarse previamente sobre la que nos interese y leer que opina la gente de ella.

           </p>
       <div class="row">
            <div class="col-lg-12">
           <?php echo Html::img("@web/imgs/fondo.jpg")?>
        </div>

    </div>
</div>
